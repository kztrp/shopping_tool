# setup.py
from distutils.core import setup

setup(name='Shopping Tool', version='0.2', author='Kamil Zdeb', author_email='235871@student.pwr.edu.pl',
      url='https://git.e-science.pl/kzdeb235871/shopping_tool',	packages=['shopping_tool', 'utils'])
