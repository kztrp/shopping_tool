class Order:
    def __init__(self, order_id, name,price, info, for_everyone):
        self.order_id = order_id
        self.name = name
        self.price = price
        self.info = info
        self.for_everyone = for_everyone

    def __str__(self) -> str:
        return "Order id: {}\nName: {}\nPrice: {}\nDecription: {}\nFor Everyone {}".format(self.order_id, self.name,
                                                                                           self.price, self.info,
                                                                                           self.for_everyone)


def fit(order, min_price, max_price, for_everyone):
    if min_price < order.price < max_price and for_everyone:
        return True
    else:
        return False



