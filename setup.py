# setup.py
from setuptools import setup, find_packages

setup(
    name='shopping_tool',
    version='0.3',
    packages=find_packages(exclude=['tests*']),
    license='MIT',
    description='shopping tool for dpp',
    long_description=open('README').read(),
    url='https://git.e-science.pl/kzdeb235871/shopping_tool',
    author='Kamil Zdeb',
    author_email='235871@student.pwr.edu.pl'
)