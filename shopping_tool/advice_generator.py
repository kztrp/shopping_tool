class AdviceGenerator:
    def __init__(self, orders):
        self.orders = orders

    def filter_orders(self, min_price, max_price, status):
        filtered_orders = []
        for order in self.orders:
            if order.fit(min_price, max_price, status):
                filtered_orders.append(order)
        return filtered_orders
