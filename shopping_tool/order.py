class Order:
    def __init__(self, order_id, name, price, info, for_everyone):
        self.order_id = order_id
        self.name = name
        self.price = price
        self.info = info
        self.status = for_everyone

    def __str__(self) -> str:
        return "Order id: {}\nName: {}\nPrice: {}\nDecription: {}\nStatus {}".format(self.order_id, self.name,
                                                                                           self.price, self.info,
                                                                                           self.status)

    def fit(self, min_price, max_price, status):
        if min_price < self.price < max_price and self.status == status:
            return True
        else:
            return False



